﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Data;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private const string DataFileName = "customers.xml";
        private const int RecordsCount = 100000;
        private const int ThreadCount = 4;

        private static string dataFilePath;
        private static string generatorFilePath;

        private static void Main(string[] args)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            StartupSetup(args);

            //Change DbContext here
            var dbContextBuilder = new DbContextBuilder<SqliteDbContext>();
            //var dbContextBuilder = new DbContextBuilder<NpgsqlDbContext>();

            new DbInitializer(dbContextBuilder.Create()).Initialize();

            GenerateCustomersDataFile();

            var customers = XmlParser.GetAllCustomers(dataFilePath);

            var loader = new CustomerDataLoader(dbContextBuilder, customers, ThreadCount);

            loader.LoadData();
        }

        private static void GenerateCustomersDataFile()
        {
            if (!string.IsNullOrEmpty(generatorFilePath))
            {
                if (!File.Exists(generatorFilePath))
                {
                    Console.WriteLine($"Assembly file with generator not found: {generatorFilePath}");
                    return;
                }

                Process.Start(generatorFilePath, $"{dataFilePath} {RecordsCount}");
            }
            else
            {
                var xmlGenerator = new XmlGenerator(dataFilePath, RecordsCount);

                xmlGenerator.Generate();
            }

            Thread.Sleep(10000);
        }

        private static void StartupSetup(string[] args)
        {
            if (args != null)
            {
                dataFilePath = args[0];
            }

            if (args != null && args.Length > 1)
            {
                generatorFilePath = args[1];
            }

            dataFilePath = Path.IsPathRooted(DataFileName)
                ? $"{DataFileName}"
                : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"{DataFileName}");
        }

        public static void Test_Different_Threads()
        {
            StartupSetup(null);

            var dbContextBuilder = new DbContextBuilder<SqliteDbContext>();
            // var dbContextBuilder = new DbContextBuilder<NpgsqlDbContext>();

            var customers = XmlParser.GetAllCustomers(dataFilePath);

            var countArray = new[] { 2, 4, 8, 16, 32, 64 };
            var messages = new List<string>();
            foreach (var threadCount in countArray)
            {
                using (var dbContext = dbContextBuilder.Create())
                {
                    new DbInitializer(dbContext).Initialize();
                }
                var loader = new CustomerDataLoader(dbContextBuilder, customers, threadCount);

                var stopWatch = new Stopwatch();
                stopWatch.Start();
                loader.LoadData();
                stopWatch.Stop();
                var message = $"Threads: {threadCount} - time: {stopWatch.Elapsed}\r\n";
                messages.Add(message);
            }
            Console.WriteLine(messages.Aggregate((m, f) => m + f));
        }
    }
}


