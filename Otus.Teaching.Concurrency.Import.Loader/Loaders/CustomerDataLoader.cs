using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Loggers;
using Otus.Teaching.Concurrency.Import.Loader.Schedulers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class CustomerDataLoader
        : IDataLoader
    {
        private readonly IDbContextBuilder<DbContext> dbContextBuilder;
        private readonly List<Customer> customers;
        private readonly int threadCount;

        public CustomerDataLoader(IDbContextBuilder<DbContext> dbContextBuilder, List<Customer> customers, int threadCount = 4)
        {
            this.dbContextBuilder = dbContextBuilder;
            this.customers = customers;
            this.threadCount = threadCount;
        }

        public void LoadData()
        {
            var stopWatch = new Stopwatch();

            Console.WriteLine("Loading data...");

            stopWatch.Start();

            var threadPoolScheduler = new ThreadPoolScheduler<Customer>(customers, threadCount);

            threadPoolScheduler.ProcessQueue(Load);

            stopWatch.Stop();

            var errorCount = CustomerImportLogger.GetErrors().Count;

            using var repository = new CustomerRepository(dbContextBuilder.Create());
            var loadedCount = repository.Count();

            Console.WriteLine($"Loading data with {threadCount} chunks finished in {stopWatch.Elapsed}");
            Console.WriteLine($"Loaded {loadedCount} from {customers.Count} records");
            Console.WriteLine($"Found {errorCount} errors\n\r");
        }

        private void Load(object item)
        {
            CustomerImportLogger.Message($"Running new thread with Id: {Thread.CurrentThread.ManagedThreadId}");

            var schedulerItem = (ThreadPoolSchedulerItem)item;

            var customers = (List<Customer>)(schedulerItem.Item);

            var retry = new Retry();

            while (!TryAddCustomers(customers))
            {
                if (!retry.NextAttempt())
                    break;
            }

            var autoResetEvent = (AutoResetEvent)schedulerItem.WaitHandle;

            autoResetEvent.Set();
        }

        private bool TryAddCustomers(IEnumerable<Customer> customers)
        {
            try
            {
                using (var repository = new CustomerRepository(dbContextBuilder.Create()))
                {
                    repository.AddCustomers(customers);

                    return true;
                }
            }
            catch (Exception ex)
            {
                CustomerImportLogger.Error($"AddCustomers exception, thread id: {Thread.CurrentThread.ManagedThreadId}", ex);
                return false;
            }
        }
    }

}