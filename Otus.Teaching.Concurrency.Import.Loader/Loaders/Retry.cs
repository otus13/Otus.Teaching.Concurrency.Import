﻿using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class Retry
    {
        private readonly int[] delayArray = { 0, 0, 0, 10, 100, 500, 2000, 4000 };
        private int retryNumber;

        public bool NextAttempt()
        {
            retryNumber++;

            if (retryNumber == 1) return true;

            if (retryNumber == delayArray.GetUpperBound(0)) return false;

            Thread.Sleep(delayArray[retryNumber]);

            return true;
        }
    }
}
