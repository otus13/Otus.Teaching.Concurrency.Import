﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Schedulers
{
    public class ThreadPoolScheduler<T>
    {
        private static int workerThreadsCount;
        private static int completionPortThreadsCount;

        private List<ThreadPoolSchedulerItem> Items { get; }

        private static void SetMinThreads(int workerThreadsCount)
        {
            ThreadPool.GetMinThreads(out workerThreadsCount, out completionPortThreadsCount);
            ThreadPool.SetMinThreads(workerThreadsCount, workerThreadsCount);
        }

        private static void RestoreMinThreads()
        {
            ThreadPool.SetMinThreads(workerThreadsCount, completionPortThreadsCount);
        }


        public ThreadPoolScheduler(List<T> items, int threadCount)
        {
            if (threadCount > 8)
                SetMinThreads(threadCount);

            var chunkLength = (int)Math.Ceiling((double)items.Count / threadCount);

            var threadPoolItems = new List<ThreadPoolSchedulerItem>();

            for (var i = 0; i < items.Count; i += chunkLength)
            {
                var chunk = items.Skip(i).Take(chunkLength).ToList();

                threadPoolItems.Add(new ThreadPoolSchedulerItem(chunk));
            }

            Items = threadPoolItems;
        }

        public void ProcessQueue(WaitCallback waitCallback)
        {

            Items.ForEach(item => ThreadPool.QueueUserWorkItem(waitCallback, item));

            WaitHandle[] waitHandles = Items.Select(handle => handle.WaitHandle).ToArray();

            WaitHandle.WaitAll(waitHandles);

            Console.WriteLine("Processing queue finished");

            RestoreMinThreads();
        }
    }
}
