﻿using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Schedulers
{
    public class ThreadPoolSchedulerItem
    {
        public int ErrorCount { get; set; }
        public object Item { get; private set; }

        public WaitHandle WaitHandle { get; private set; }

        public ThreadPoolSchedulerItem(object item)
        {
            Item = item;

            WaitHandle = new AutoResetEvent(false);
        }
    }
}
