using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository, IDisposable
    {
        private readonly DbContext context;

        public CustomerRepository(DbContext dbContext)
        {
            context = dbContext;
        }

        public void AddCustomer(Customer customer)
        {
            context.Add(customer);

            context.SaveChanges();
        }

        public void AddCustomers(IEnumerable<Customer> customers)
        {
            context.AddRange(customers);

            context.SaveChanges();
        }

        public int Count()
        {
            return context.Set<Customer>().Count();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}