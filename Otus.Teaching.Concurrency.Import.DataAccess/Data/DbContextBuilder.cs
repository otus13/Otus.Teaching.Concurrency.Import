﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    public class DbContextBuilder<T> : IDbContextBuilder<T> where T : DbContext
    {
        public T Create()
        {
            var context = (T)Activator.CreateInstance<T>();

            return context;
        }
    }
}
