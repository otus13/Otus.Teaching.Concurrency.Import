﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    public class DbInitializer : IDisposable
    {
        private readonly DbContext dbContext;

        public DbInitializer(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Initialize()
        {
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }
    }
}
