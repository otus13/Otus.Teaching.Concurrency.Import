﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Loader.Loggers
{
    public static class CustomerImportLogger
    {
        private static readonly List<(string, Exception)> errorItems = new List<(string, Exception)>();
        private static readonly List<string> messages = new List<string>();

        public static List<(string, Exception)> GetErrors() => errorItems;

        public static List<string> GetMessages() => messages;

        public static void Message(string message)
        {
            messages.Add(message);
        }

        public static void Error(string message, Exception ex)
        {
            errorItems.Add((message, ex));
        }
    }
}
