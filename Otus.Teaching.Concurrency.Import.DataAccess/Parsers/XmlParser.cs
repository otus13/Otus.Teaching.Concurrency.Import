﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly string dataFilePath;
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(CustomersList));

        public XmlParser(string dataFilePath)
        {
            this.dataFilePath = dataFilePath;
        }

        public List<Customer> Parse()
        {
            List<Customer> customers;

            //Parse data
            using (var fileStream = new FileStream(dataFilePath, FileMode.Open))
            {
                var customersList = ((CustomersList)Serializer.Deserialize(fileStream));

                customers = customersList.Customers.Select(c => new Customer()
                {
                    Id = c.Id,
                    FullName = c.FullName,
                    Email = c.Email,
                    Phone = c.Phone
                }).ToList();
            }

            return customers;
        }

        public static List<Customer> GetAllCustomers(string dataFilePath)
        {
            var xmlParser = new XmlParser(dataFilePath); //TODO: Add file watcher

            var customers = xmlParser.Parse();

            return customers;
        }
    }
}