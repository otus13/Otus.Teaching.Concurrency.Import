﻿namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface IDbContextBuilder<out T>
    {
        T Create();
    }
}
