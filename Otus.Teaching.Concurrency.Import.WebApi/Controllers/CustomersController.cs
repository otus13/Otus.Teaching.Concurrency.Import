﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.DataAccess.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebApi.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly SqliteDbContext _sqliteDbContext;

        public CustomersController(ILogger<CustomersController> logger, SqliteDbContext sqliteDbContext)
        {
            _logger = logger;
            _sqliteDbContext = sqliteDbContext;
        }


        [HttpGet("{id:long}")]
        public async Task<ActionResult<CustomerResponse>> GetById(long id)
        {
            var customer = await _sqliteDbContext.Customers.FirstOrDefaultAsync(c => c.Id == id);

            if (customer is null)
                return NotFound();

            return Ok(new CustomerResponse(customer));
        }

        [HttpPost]
        public async Task<ActionResult> Add(CustomerRequest request)
        {
            var customer = await _sqliteDbContext.Customers.FirstOrDefaultAsync(c => c.Id == request.Id);

            if (customer is not null)
                return Conflict();

            _sqliteDbContext.Customers.Add(new Customer()
            {
                Id = request.Id,
                Email = request.Email,
                FullName = request.FullName,
                Phone = request.Phone
            });

            await _sqliteDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
