﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.WebApi.Models
{
    public class CustomerResponse
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            FullName = customer.FullName;
            Email = customer.Email;
            Phone = customer.Phone;
        }
    }
}
