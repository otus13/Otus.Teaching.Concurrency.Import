﻿namespace Otus.Teaching.Concurrency.Import.WebApi.Models
{
    public class CustomerRequest
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
