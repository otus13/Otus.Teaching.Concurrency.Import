﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;


namespace Otus.Teaching.Concurrency.Import.Client
{
    public class CustomerService
    {
        private static HttpClient _client;
        private static Uri _baseUri;

        public CustomerService(string baseUriString)
        {
            _client = new HttpClient();
            _baseUri = new Uri(baseUriString);
        }

        public async Task<string> GetByIdAsync(int id)
        {
            var response = await _client.GetAsync(new Uri(_baseUri, $"customers/{id}"));

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> CreateAsync(Customer customer)
        {
            var response = await _client.PostAsJsonAsync(new Uri(_baseUri, $"customers"), customer);

            if (response.IsSuccessStatusCode)
                return "Customer created";

            return $"Customer creation failed. Status code: {response.StatusCode}";
        }
    }
}
