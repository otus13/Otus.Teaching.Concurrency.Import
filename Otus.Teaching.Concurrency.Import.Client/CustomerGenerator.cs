﻿using Bogus;

namespace Otus.Teaching.Concurrency.Import.Client
{
    public class CustomerGenerator
    {
        private Faker<Customer> _customersFaker;
        public int InitialId { get; private set; }


        public void SetInitialId(int id)
        {
            InitialId = id;

            _customersFaker = CreateFaker();
        }

        public Customer Create()
        {
            return _customersFaker.Generate();
        }

        private Faker<Customer> CreateFaker()
        {
            var id = InitialId;
            var customersFaker = new Faker<Customer>()
                .CustomInstantiator(f => new Customer()
                {
                    Id = id++
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
    }
}
