﻿using System;

namespace Otus.Teaching.Concurrency.Import.Client
{
    public class InputResult
    {
        public static readonly string ExitCode = "q";
        public static readonly string AddCustomerCode = "a";

        public static bool IsExit(string input)
        {
            return input.Equals(ExitCode, StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool IsAddCustomer(string input)
        {
            return input.Equals(AddCustomerCode, StringComparison.CurrentCultureIgnoreCase);
        }

        internal static bool GetCustomerId(string input, out int customerId)
        {
            return int.TryParse(input, out customerId);
        }
    }
}
