﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Client
{
    class Program
    {
        private static string _baseUri = "http://localhost:5000";

        static async Task Main(string[] args)
        {
            var customerService = new CustomerService(_baseUri);
            var customerGenerator = new CustomerGenerator();


            while (true)
            {
                Console.WriteLine(
                    $"Enter customer id to get Customer, Type '{InputResult.AddCustomerCode}' to create new Customer " +
                    $"or type '{InputResult.ExitCode}' to exit: ");

                var input = Console.ReadLine();

                if (InputResult.IsExit(input))
                    break;

                if (InputResult.IsAddCustomer(input))
                {
                    if (customerGenerator.InitialId == 0)
                    {
                        Console.WriteLine("Enter initial id for customer generator: ");
                        int initialId;
                        while (true)
                        {
                            var inputId = Console.ReadLine();
                            if (int.TryParse(inputId, out initialId))
                                break;

                            Console.WriteLine("Incorrect input format");
                        }
                        customerGenerator.SetInitialId(initialId);
                    }
                    var result = await customerService.CreateAsync(customerGenerator.Create());
                    Console.WriteLine(result);
                }
                else if (!InputResult.GetCustomerId(input, out var customerId))
                    Console.WriteLine("Incorrect input format");
                else
                    Console.WriteLine(await customerService.GetByIdAsync(customerId));
            }

            Console.WriteLine("Good bye!");
        }
    }
}
